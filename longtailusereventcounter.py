from usereventcounter import UserEventCounter
from tl.utils import create_class

class BloomFilterFactory(object):

    @classmethod
    def create_filter(cls, module_name):
        return create_class(module_name) 

class LongTailUserEventCounter(UserEventCounter):
    """ stores single events for a user id in a bloom filter 
    more efficient for large number of single events 
    """

    TAIL_ID = "<tail>"
    BLOOM_FILTER = "tl.pybloom.redispybloom.RedisBloomFilter"
    FILTER_SIZE = 10**6
    ERROR_RATE = 10**-8

    def __init__(self, config, domain, tokenifier, tailevents):
        """ inherits from UserEventCounter 
        Args: 
            config: config for key value store
            domain: string to namespace the values in the key value store
            tokenifier: map string to shorter token
            tailevents: list of signatures that trigger user ids to be kept in bloom filter
                           usually somethings that generates a large number of singular events
                           e.g. [('visit', 'sport'), ('share', 'politics']

        Original get, set and merge function are overridden to add bloom filter functionality
        """

        super(LongTailUserEventCounter, self).__init__(config, domain, tokenifier)
        self._bloom_filter = None
        self._tail_events = tailevents

    def _get_filter_keys(self, uid, events, referrer_uid):
        is_referrer = False
        if referrer_uid:
            is_referrer = True
        for event in events:
            yield (uid, event[0], event[1], is_referrer)

    def get(self, uid):
        """Given a user ID returns all counts for that user.
           checks also bloom filter
        
        Args:
            uid: uuid string representing the user ID.
    
        Returns:
            Dictionary keyed by tuples containing the count data.
            
            Each item has the form:
              ( signature, is_viral ): { category1: count1, category2: count2, ... }
              
            Example response:
            [
              { ("shares", False): { "fashion": 1, "__total__": 2, "__unknown__": 1 },
                ("shares", True): { "fashion", 3, "__total__": 3 },
                ("visits", False): { "politics": 3 } }
            ]
        """

        results = {}
        # first: look for results in redis store
        results = super(LongTailUserEventCounter, self).get(uid)
        # second: check bloom filter for additional events
        results = self._check_tail_events(uid, results)
        return results

    def _check_tail_events(self, uid, results):
        for (sig, cat) in self._tail_events:
            for is_ref in (True, False):
                test_bloom = self._bloom_filter_keygen(uid, sig, cat, is_ref)
                if (test_bloom, sig) in self.bloom_filter:
                    results = self._update_results(results, (sig, is_ref), {cat: 1, "__total__": 1})
        return results

    def _update_results(self, results, event, update):
        if results:
            for k, v in update.items():
                # ignore events that are already present in results!!
                if not k == '__total__' and not k in results[event]:
                    results[event].update({k: v})
                    results[event]['__total__'] = results[event]['__total__'] + v
        else:
            results[event] = update
        return results
            

    def _bloom_filter_keygen(self, uid, signature, category, is_referrer):
        if not (signature, category) in self._tail_events:
            return None
        if is_referrer:
            return "%s:%s+" % (uid, category)
        else:
            return "%s:%s" % (uid, category)

    def increment(self, uid, events, is_referrer):
        """ Increment counters and add uid to filter if appears for the first time
        
        Args:
            uid: A uuid string representing the id of the user on whose behalf
                we are counting.
            events: A list of tuple triplets representing the events we will count.
                The first part of the tuple is the signature of the event. The
                second part of the tuple is the category of the event and the
                third part is the amount to increment.
                Counts will be stored in the kvstore using a key formed from the
                namespace, domain, uid and signature. Each entry in the kvstore
                is a hashmap with the category at the map key and the count in
                the value.
                
                Note that if a category is None it will be increment
                it will increment a counter for the category "__unknown__".
                
                Also note that is the category is set to "__total__" then the
                total count will be incremented directly and no other category
                (including unknown) will be created or updated.
                
                Example event:
                    
                    ("shares", "fashion", 1)
                    
                    The signature here "shares"
                    
                    The category is "fashion".
                    
                    The increment value is 1.
                
            is_referrer: If these events are a result of a referral
                this boolean field is set to True.
                The counters for a user's viral activity
                are separate from their non-viral counters. They are
                differentiated by appending ":+" to the kvstore key.
        """

        increment_all = []
        increment_totals_only = []
        increment_not_unique = []

        for signature, category, amount in events:
            bloom_key = self._bloom_filter_keygen(uid, signature, category, is_referrer)
            if not bloom_key:
                # sig/cat combi not in filter
                increment_all.append((signature, category, amount))
                continue
            if not (bloom_key, signature) in self.bloom_filter and amount == 1:
                # add to bloom filter
                self.bloom_filter.add(bloom_key, signature)
                increment_totals_only.append((signature, category, amount))
            elif (bloom_key, signature) in self.bloom_filter:
                # present in bloom filter
                current = self.get(uid)[(signature, is_referrer)][category]
                if current == 1:
                    # only in bloom filter but count = 1; make sure uniques are not increased!
                    increment_not_unique.append((signature, category, amount+1))
                else:
                    increment_all.append((signature, category, amount))
            elif amount > 1:
                increment_all.append((signature, category, amount))

        if increment_all:
            self._incr_counters(uid, increment_all, is_referrer)
        if increment_totals_only:
            self._incr_counters(uid, increment_totals_only, is_referrer, totals_only=True)
        if increment_not_unique:
            self._incr_counters(uid, increment_not_unique, is_referrer, is_unique=False)

    def merge(self, old_uid, new_uid):
        """ add counts from old id into new id; checks bloom filter!
            delete old id (not from bloom filter!)

        Args:
            old_uid: A uuid string representing the id of the old user.
            new_uid: A uuid string representing the id of the new user.            
        """

        old_results = {}
        new_results = {}

        # get results
        old_results = self.get(old_uid)
        new_results = self.get(new_uid)
        if old_results:
            for signature, is_referrer in old_results.keys():
                new_key = self._keygen(new_uid, signature, is_referrer)
                old_key = self._keygen(old_uid, signature, is_referrer)
                for k, v in old_results[(signature, is_referrer)].items():
                    self._store.hash_incr_by(new_key, self._tokenifier.shorten(k), v)
                # check if new uid contains results
                if (signature, is_referrer) in new_results:
                    for k, v in new_results[(signature, is_referrer)].items():
                        # only needs to be added if 1 (i.e. coming from bloom filter)!
                        if v == 1:
                            self._store.hash_incr_by(new_key, self._tokenifier.shorten(k), v)
                self._store.val_delete(old_key)

    @property
    def bloom_filter(self):
        """ creates bloom filter """
        if not self._bloom_filter:
            self._bloom_filter = BloomFilterFactory.create_filter(self.BLOOM_FILTER)(self._store,
                                                                                     self._keygen_base(self.TAIL_ID),
                                                                                     self.FILTER_SIZE,
                                                                                     self.ERROR_RATE)

        return self._bloom_filter
