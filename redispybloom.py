"""This module implements a bloom filter using redis.
It borrows heavily from the following similar implementation: 
https://github.com/jaybaird/python-bloomfilter
It fully adopts the generation of hashfunctions from
the corresponding module (see import!)
for more details see also:
http://gsd.di.uminho.pt/members/cbm/ps/dbloom.pdf
This new module here replaces the python bitarray 
with bitarray from redis using getbit/setbit functions!
There is a scalable and non-scalable variant.
"""

from pybloom import make_hashfuncs
import math

class RedisBloomFilter(object):
    """ Bloomfilter implementation using Redis"""

    def __init__(self, store, store_key, capacity, error_rate=0.001, store_count=1):
        """ constructor
        Args: 
            store: redis keyvaluestore object
                   e.g. from "tl.keyvaluestore.redis.store.RedisKeyValueStore"
            store_key: string describing the name of the bloom filter bit array in redis
                   e.g. "poc:test:<filtertest>"
            capacity: integer, maximum number of items in bloom filter
            error_rate: float, maximum allowed rate for false positives
            store_count: integer, number of filter (only important for scalable version!)
        """

        self._store = store
        self._store_key = store_key
        self._error_rate = error_rate
        self._capacity = capacity
        self._make_hashes = make_hashfuncs(self._num_slices, self._bits_per_slice)
        self._store_count = store_count
        
    def _event_key(self, signature):
        return "%s:%s:%s" % (self._store_key, signature, self._store_count)
        
    def add(self, key, signature):
        """ stores a key and a signature to the bloom filter 
        Args: 
            key: string, user id
            signature: string, event, e.g. "share" or "visit"
        Returns:
            True 
        """

        hashes = self._make_hashes(key)
        offset = 0
        for k in hashes:
            self._store.set_bit(self._event_key(signature), offset + k, 1)
            offset += self._bits_per_slice
        return True

    def __contains__(self, event):
        """ checks if key and signature is found in bloom filter
        Args:
            event: two-tuple of key and signature
                   e.g. ('aaaaaaaaaaaaaaaaaaaaaa', 'visit')

        Returns:
            True if event in bloom filter, else False
        """

        key, signature = event
        hashes = self._make_hashes(key)
        offset = 0
        for k in hashes:
            if not self._store.get_bit(self._event_key(signature), offset + k):
                return False
            offset += self._bits_per_slice
        return True

    @property
    def _num_slices(self):
        # given M = num_bits, k = num_slices, p = error_rate, n = capacity
        # solving for m = bits_per_slice
        # n ~= M * ((ln(2) ** 2) / abs(ln(P)))
        # n ~= (k * m) * ((ln(2) ** 2) / abs(ln(P)))
        # m ~= n * abs(ln(P)) / (k * (ln(2) ** 2))
        return int(math.ceil(math.log(1 / self._error_rate, 2)))

    @property
    def _bits_per_slice(self):
        # the error_rate constraint assumes a fill rate of 1/2
        # so we double the capacity to simplify the API
        #self.bits_per_slice = int(math.ceil(
        #    (2 * capacity * abs(math.log(error_rate))) /
        #    (self.num_slices * (math.log(2) ** 2))))
        return int(math.ceil(
            (2 * self._capacity * abs(math.log(self._error_rate))) /
            (self._num_slices * (math.log(2) ** 2))))


class ScalableRedisBloomFilter(RedisBloomFilter):
    """ scalable bloom filter implementation using redis 
        slower but saves more space than the non-scalable version
    """

    SCALE = 2     # bigger means faster, but takes up more memory
    RATIO = 0.9   # determines how fast error rate converges to nil (0 < ratio < 1)
    FILTER_KEY = "<bloomstore>"
    MODE = 0      # i.e. exponential scaling (set to 1 for linear growth of capacity; 2 for constant capacity)

    def __init__(self, store, store_key, initial_capacity, error_rate):
        """ constructor
        Args: 
            store: redis keyvaluestore object
                   e.g. from "tl.keyvaluestore.redis.store.RedisKeyValueStore"
            store_key: string describing the name of the bloom filter bit array in redis
                   e.g. "poc:test:<filtertest>"
            initial_capacity: integer, initial maximum number of items in bloom filter
            error_rate: float, initial maximum allowed rate for false positives
        """
        
        super(ScalableRedisBloomFilter, self).__init__(store, store_key, capacity=initial_capacity, error_rate=error_rate)
        self._init_capacity = initial_capacity

    def _filter_name(self, num_filters, num_items):
        if self.MODE == 0: # fastest
            capacity =  self._init_capacity * (self.SCALE ** (num_filters-1))
        elif self.MODE == 1: # slower than 0
            capacity = self._init_capacity + self.SCALE * self._init_capacity * (num_filters-1)
        elif self.MODE == 2: # slowest
            capacity = self._init_capacity
        
        return "%s:%s:%s:%s" % (num_filters, 
                                capacity, 
                                self._error_rate * (self.RATIO ** (num_filters-1)),
                                num_items)

    def _filter_add_right(self, filter_name):
         self._store.list_rpush(self._bloomstore, filter_name)

    def _filter_add_left(self, filter_name):
         self._store.list_lpush(self._bloomstore, filter_name)

    def _set_hashes(self, capacity, error_rate):
        self._capacity = int(capacity)
        self._error_rate = float(error_rate)
        self._make_hashes = make_hashfuncs(self._num_slices, self._bits_per_slice)        

    def add(self, key, signature):
        """ adds key, signature to filter, adds new filter is old one is full
        Args: 
            key: string, user id
            signature: string, event, e.g. "share" or "visit"
        Returns:
            True 
        """

        # get current filter for storage
        current_filter = self._current_filter
        if not current_filter:
            self._filter_add_right(self._filter_name(self._store_count, 0))
            current_filter = self._current_filter
        (store_count, capacity, error_rate, item_count) = current_filter.split(':')
        # set correct hash functions
        self._set_hashes(int(capacity), float(error_rate))
        super(ScalableRedisBloomFilter, self).add(key, signature)
        item_count = int(item_count) + 1
        if item_count < self._capacity:
            # put filter back if not full
            self._filter_add_right("%s:%s:%s:%s" % (store_count, capacity, error_rate, item_count))
        else:
            # if full put back and add new filter
            self._filter_add_right("%s:%s:%s:%s" % (store_count, capacity, error_rate, item_count))
            item_count = 0
            self._store_count = int(store_count) + 1
            self._filter_add_right(self._filter_name(self._store_count, 0))
        return True

    def __contains__(self, event):
        """ checks if key and signature is found across multiple bloom filters
        Args:
            event: two-tuple of key and signature
                   e.g. ('aaaaaaaaaaaaaaaaaaaaaa', 'visit')

        Returns:
            True if event if found in one bloom filter, else False
        """

        key, signature = event
        contains = False
        # check all filters
        for f in range(self._all_filters):
            current_filter = self._current_filter
            (store_count, capacity, error_rate, item_count) = current_filter.split(':')
            # set correct hash functions
            self._set_hashes(int(capacity), float(error_rate))
            self._store_count = int(store_count)
            if super(ScalableRedisBloomFilter, self).__contains__((key, signature)):
                self._filter_add_left(current_filter)  
                # make sure all filters are in the correct order for next search!
                for _ in range(f+1, self._all_filters):
                    current_filter = self._current_filter
                    self._filter_add_left(current_filter)  
                return True
            self._filter_add_left(current_filter)           
        if contains:
            return True
        else:
            return False        

    @property
    def _all_filters(self):
        return self._store.list_len(self._bloomstore)

    @property
    def _current_filter(self):
        return self._store.list_rpop(self._bloomstore)

    @property
    def _bloomstore(self):
        return "%s:%s" % (self._store_key, self.FILTER_KEY)
