import unittest
from tl.test.framework.testcase import TestCase
import settings
from tl.tokenifier import Tokenifier
from tl.usereventcounter import UserEventCounter
from tl.longtailusereventcounter import LongTailUserEventCounter
import decimal
import datetime
import uuid
import time

class TestLongTailUserEventCounter(TestCase):
    
    MOCK_DOMAIN = "test"
    # events for storage in bloomfilter: to be specified in client config!
    TAIL_EVENTS = [('share', 'politics'), ('share', 'sports')]

    def setUp(self):
        self.redis.flush()
    
    def instance(self):
        tokenifier = Tokenifier(settings.TokenifyConfig, self.MOCK_DOMAIN)
        return LongTailUserEventCounter(settings.UserEventCounterConfig, 
                                        self.MOCK_DOMAIN, 
                                        tokenifier,
                                        self.TAIL_EVENTS)

    def test_tail_counter(self):

        tail_counter = self.instance()

        uids = ["a"*22, "b"*22, "c"*22, "d"*22]
        signature = "share"
        is_referrer = False
        categories = ["sports", "comedy", "politics"]
        
        # add some uids, event combis into bloom filter
        check_keys = []
        for uid in uids:
            for cat in categories:
                tail_counter.increment(uid, [(signature, cat, 1)])
                # remember events
                if (signature, cat) in self.TAIL_EVENTS and uid not in check_keys:
                    check_keys.append((uid, tail_counter._bloom_filter_keygen(uid, signature, cat, is_referrer)))


        # check if events are in bloom filter and counts are correct
        for uid, key in check_keys:
            for signature, category in self.TAIL_EVENTS:
                self.assertTrue((key, signature) in tail_counter.bloom_filter)
            result = tail_counter.get(uid)
            total_count = 0
            for k, v in result[(signature, is_referrer)].items():
                if not k == '__total__':
                    self.assertTrue(v == 1)
                    total_count += 1
            self.assertTrue(result[(signature, is_referrer)]['__total__'] == total_count)

        # make sure counter still gets incremented
        event_type = 'sports'
        event_count = 20
        tail_counter.increment("a"*22, [(signature, event_type, event_count)])
        result = tail_counter.get("a"*22)
        # check if counters are increased by event count
        self.assertTrue(result[(signature, is_referrer)]['__total__'] == event_count + 3)
        self.assertTrue(result[(signature, is_referrer)]['sports'] == event_count + 1)

        # check some keys that are not in the filter
        check_wrong_keys = ["e"*22, "f"*22]
        for key in check_wrong_keys:
            self.assertFalse(tail_counter.get(key))

            
        # check if uid gets past bloom filter if not present
        tail_counter.increment("e"*22, [(signature, event_type, event_count)])
        self.assertTrue(tail_counter.get("e"*22)[(signature, is_referrer)][event_type] == event_count)
        self.assertTrue(tail_counter.get("e"*22)[(signature, is_referrer)]['__total__'] == event_count)
        # check that uid is not in bloom filter
        self.assertFalse(("e"*22, (signature, event_type, is_referrer)) in tail_counter.bloom_filter)
        # now add bloom filtered event
        tail_counter.increment("e"*22, [('share', 'politics', 1)])
        # check if uid is in bloom filter and count is 1
        bloom_key = tail_counter._bloom_filter_keygen("e"*22, 'share', 'politics', is_referrer)
        self.assertTrue((bloom_key, 'share') in tail_counter.bloom_filter)
        self.assertTrue(tail_counter.get("e"*22)[('share', is_referrer)]['politics'] == 1)

    def test_counter_increment(self):

        counter = self.instance()

        signature = "share"
        is_referrer = False
        for _ in range(5):
            uid = "a"*22
            counter.increment(uid, [(signature, 'sports', 1)])
            self.assertTrue(counter.get(uid)[(signature, is_referrer)]['__total__'] == _ + 1)
            self.assertTrue(counter.get(uid)[(signature, is_referrer)]['sports'] == _ + 1)
        
    def test_tail_totals(self):

        counter = self.instance()

        signature = "share"
        is_referrer = False
        for _ in range(5):
            uid = "a"*22
            counter.increment(uid, [(signature, 'sports', 1)])
        for _ in range(4):
            uid = "b"*22
            counter.increment(uid, [(signature, 'sports', 1)])
        for _ in range(7):
            uid = "c"*22
            counter.increment(uid, [(signature, 'sports', 1)])
        for _ in range(9):
            uid = "d"*22
            counter.increment(uid, [(signature, 'sports', 1)])

        user_co = decimal.Decimal(4)
        sums = decimal.Decimal(5 + 4 + 7 + 9)
        sum_sq = decimal.Decimal(5*5 + 4*4 + 7*7 + 9*9)
        mean = sums/user_co
        var = sum_sq / user_co - mean * mean
        sigmas = counter._get_sigma_range(signature, "__total__", is_referrer)
        self.assertTrue(mean == sigmas[0])
        self.assertTrue(var == sigmas[1])

    def test_tail_merge_not_from_bloom(self):
        
        # test with counts > 1, i.e. in redis store

        counter = self.instance()

        signature = "share"
        is_referrer = False
        for _ in range(5):
            old_uid = "a"*22
            counter.increment(old_uid, [(signature, 'sports', 1)])

        for _ in range(5):
            new_uid = "b"*22
            counter.increment(new_uid, [(signature, 'sports', 1)])

        old_results = counter.get(old_uid)
        new_results = counter.get(new_uid)
        counter.merge(old_uid, new_uid)
        merge_results = counter.get(new_uid)
        self.assertTrue(merge_results[(signature, is_referrer)]['__total__'] ==  old_results[(signature, is_referrer)]['__total__'] + new_results[(signature, is_referrer)]['__total__'])
        self.assertTrue(merge_results[(signature, is_referrer)]['sports'] ==  old_results[(signature, is_referrer)]['sports'] + new_results[(signature, is_referrer)]['sports'])

    def test_tail_merge_both_from_bloom(self):

        # test counts == 1, i.e. both in bloom filter

        counter = self.instance()

        old_uid = "a"*22
        new_uid = "b"*22

        signature = "share"
        is_referrer = False
        counter.increment(old_uid, [(signature, 'sports', 1)])
        counter.increment(new_uid, [(signature, 'sports', 1)])

        old_results = counter.get(old_uid)
        new_results = counter.get(new_uid)
        counter.merge(old_uid, new_uid)
        merge_results = counter.get(new_uid)
        self.assertTrue(merge_results[(signature, is_referrer)]['__total__'] ==  old_results[(signature, is_referrer)]['__total__'] + new_results[(signature, is_referrer)]['__total__'])
        self.assertTrue(merge_results[(signature, is_referrer)]['sports'] ==  old_results[(signature, is_referrer)]['sports'] + new_results[(signature, is_referrer)]['sports'])

    def test_tail_merge_only_new_from_bloom(self):

        # test counts of new is in bloom filter and the old is not
        counter = self.instance()

        old_uid = "a"*22
        new_uid = "b"*22

        signature = "share"
        is_referrer = False
        counter.increment(old_uid, [(signature, 'sports', 10)])
        counter.increment(new_uid, [(signature, 'sports', 1)])

        old_results = counter.get(old_uid)
        new_results = counter.get(new_uid)
        counter.merge(old_uid, new_uid)
        merge_results = counter.get(new_uid)
        self.assertTrue(merge_results[(signature, is_referrer)]['__total__'] ==  old_results[(signature, is_referrer)]['__total__'] + new_results[(signature, is_referrer)]['__total__'])
        self.assertTrue(merge_results[(signature, is_referrer)]['sports'] ==  old_results[(signature, is_referrer)]['sports'] + new_results[(signature, is_referrer)]['sports'])

    def test_tail_merge_only_old_from_bloom(self):

        # test counts of old is in bloom filter and the new is not
        counter = self.instance()

        old_uid = "a"*22
        new_uid = "b"*22

        signature = "share"
        is_referrer = False
        counter.increment(old_uid, [(signature, 'sports', 1)])
        counter.increment(new_uid, [(signature, 'sports', 10)])

        old_results = counter.get(old_uid)
        new_results = counter.get(new_uid)
        counter.merge(old_uid, new_uid)
        merge_results = counter.get(new_uid)
        self.assertTrue(merge_results[(signature, is_referrer)]['__total__'] ==  old_results[(signature, is_referrer)]['__total__'] + new_results[(signature, is_referrer)]['__total__'])
        self.assertTrue(merge_results[(signature, is_referrer)]['sports'] ==  old_results[(signature, is_referrer)]['sports'] + new_results[(signature, is_referrer)]['sports'])


if __name__ == "__main__":
    unittest.main()
