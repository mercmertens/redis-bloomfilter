import unittest
from tl.test.framework.testcase import TestCase
from tl.pybloom.redispybloom import RedisBloomFilter
from tl.pybloom.redispybloom import ScalableRedisBloomFilter
from tl.keyvaluestore.factory import KeyValueStoreFactory
import settings
import uuid
import time

class TestRedisBloomFilter(TestCase):

    def setUp(self):
        self.redis.flush()

    def instance(self, rate, capacity=None, init_capacity=None):
        store = KeyValueStoreFactory.build(settings.UserEventCounterConfig)
        if capacity:
            return RedisBloomFilter(store, "poc:test:<filtertest>", capacity, rate)
        elif init_capacity:
            return ScalableRedisBloomFilter(store, "poc:test:<filtertest>", init_capacity, rate)
        else:
            return None

    def test_filter_nonscaling(self):
        
        print "NON-SCALING"

        test_count = 10000
        capacity = 100000
        rate = 0.01

        # setup
        f = self.instance(rate, capacity=capacity)
        
        # test biggish number
        excluded = []
        included = []
        for i in range(test_count):
            excluded.append(uuid.uuid4().hex)
            included.append(uuid.uuid4().hex)
            
        for uid in included:
            f.add(uid, 'test')

        start = time.time()
        positive = 0
        for uid in included:
            if (uid, 'test') in f:
                positive += 1
        print "CHECKED TRUE POSITIVE PER SEC", test_count / (time.time() - start)

        self.assertTrue(positive == test_count)
 
        start = time.time()
        false_positive = 0
        for uid in excluded:
            if (uid, 'test') in f:
                false_positive += 1
        print "CHECKED FALSE POSITIVE PER SEC", test_count / (time.time() - start)

        self.assertTrue(float(false_positive)/test_count <= rate)

    def test_filter_scaling_exp_2(self):
        
        print "\nSCALING EXPONENTIAL SCALE 2"

        rate = 0.01
        init_capacity = 1000
        test_count = 10000

        f = self.instance(rate, init_capacity=init_capacity)
        self.run_scaling(f, test_count, rate)

    def test_filter_scaling_exp_4(self):
        
        print "\nSCALING EXPONENTIAL SCALE 4"

        rate = 0.01
        init_capacity = 1000
        test_count = 10000

        f = self.instance(rate, init_capacity=init_capacity)
        f.SCALE = 4
        self.run_scaling(f, test_count, rate)

    def test_filter_scaling_lin_2(self):
        
        print "\nSCALING LINEAR SCALE 2"

        rate = 0.01
        init_capacity = 1000
        test_count = 10000

        f = self.instance(rate, init_capacity=init_capacity)
        f.MODE = 1
        self.run_scaling(f, test_count, rate)

    def test_filter_scaling_lin_10(self):
        
        print "\nSCALING LINEAR SCALE 10"

        rate = 0.01
        init_capacity = 1000
        test_count = 10000

        f = self.instance(rate, init_capacity=init_capacity)
        f.MODE = 1
        f.SCALE = 10
        self.run_scaling(f, test_count, rate)

    def run_scaling(self, f, test_count, rate):

        signature = 'test'
        included = []
        excluded = []

        for _ in range(test_count):
            included.append(uuid.uuid4().hex)
            excluded.append(uuid.uuid4().hex)

        for key in included:
            f.add(key, signature)
            
        start = time.time()
        true_positive = 0
        for key in included:
            if (key, signature) in f:
                true_positive += 1
        print "CHECKED TRUE POSITIVE PER SEC", test_count / (time.time() - start)

        self.assertTrue(true_positive == test_count)

        start = time.time()
        false_positive = 0
        for key in excluded:
            if (key, signature) in f:
                false_positive += 1
        print "CHECKED FALSE POSITIVE PER SEC", test_count / (time.time() - start)

        self.assertTrue(float(false_positive)/test_count <= rate)


if __name__ == '__main__':

    unittest.main()
